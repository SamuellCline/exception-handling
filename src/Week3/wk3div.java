package Week3;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import java.util.InputMismatchException;

public class Wk3div {
    //@Positive //these two do the same thing in the case
    @Max(value = 9999)
    @Min(value= -9999)
    public float numerator;
    //@Positive
    @Max(value = 9999)
    @Min(value= -9999)
    public float denominator;



    public static float division(float a, float b){

        if(a != 0 && b != 0) {

            return a / b;

        }
        else{
            throw new InputMismatchException();


        }

    }

}
