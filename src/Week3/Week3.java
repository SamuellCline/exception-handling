package Week3;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.InputMismatchException;
import java.util.Scanner;
import java.util.Set;


public class Week3 {

  public static void main(String[] args) {
      Wk3div numbers = new Wk3div();
      boolean done = false;
      while (!done){

      Scanner input = new Scanner(System.in);
          try {
      System.out.println("Please enter Numerator:");

      numbers.numerator = input.nextFloat(); //Java allows doubles to have a zero as the numerator/denominator.
      System.out.println("Please enter Denominator:");
      numbers.denominator = input.nextFloat();//The results are "infinity" and "Not a number", eliminating that exception.
              ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
              Validator validator = factory.getValidator();
              Set<ConstraintViolation<Wk3div>> violations = validator.validate(numbers);
              for (ConstraintViolation<Wk3div> violation : violations) {
                  System.out.println(violation.getMessage());

              }
                if(violations.isEmpty()){
                    System.out.print(Wk3div.division(numbers.numerator, numbers.denominator));
                    done = true;
                }

              else {
                    throw new InputMismatchException();
                }
          } catch (ArithmeticException | InputMismatchException e) {

              System.out.println("Please enter a valid input");

          }


      }
  }




}
